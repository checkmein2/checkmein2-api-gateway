FROM node:7.7.3-alpine
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN yarn install
CMD [ "yarn", "start" ]
ENV PORT 8000
EXPOSE 8000