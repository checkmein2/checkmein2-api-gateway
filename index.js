const express = require('express');
const logger = require('./src/utils/logger.js');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const requestId = require('cc-request-id');
const httpProxy = require('http-proxy');
const Config = require('./src/config/config.js');
const health = require('./src/api/health.js');
const fs = require('fs');
const app = express();

const port = process.env.PORT || 8000;
const proxyServer = httpProxy.createProxyServer({});

if (!fs.existsSync('logs')) {
    fs.mkdirSync('logs');
}

app.use(cors());
app.use(helmet());
health(app);
// Add unique request id and correlation id for each request hitting the server
app.use(requestId({ secret: Config.SERVER_SECRET }));
app.use(morgan('combined'));
app.use((req, res) => {
    proxyServer.web(req, res, {
        target: `http://${Config.APPLICATION_ROUTER_HOST}:${Config.APPLICATION_ROUTER_PORT}`
    });
});

app.listen(port, () => {
    logger.info(`Starting up API Gateway on port ${port}...`);
});
