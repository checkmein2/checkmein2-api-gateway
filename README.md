# API gateway

This is the only component in the system that knows about the micro-services.
It's responsability is to handle cross cutting concerns such as:

* CORS
* Security
* Request logging( and also setting up request and correlation ids )
* Abstraction of the actual endpoints used.