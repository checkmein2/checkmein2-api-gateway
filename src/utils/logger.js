const winston = require('winston');
// eslint-disable-next-line
require('winston-papertrail').Papertrail;

const logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'info',
            handleExceptions: true,
            filename: 'logs/all.log',
            json: true,
            maxsize: 5242880, // 5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        }),
        new winston.transports.Papertrail({
            host: 'logs5.papertrailapp.com',
            port: 39491
        })
    ],
    exitOnError: false
});

module.exports = logger;
