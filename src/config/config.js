module.exports = {
    APPLICATION_ROUTER_HOST: process.env.APPLICATION_ROUTER_URL || 'localhost',
    APPLICATION_ROUTER_PORT: process.env.APPLICATION_ROUTER_PORT || 3100,
    LOG_URL: process.env.LOG_URL || 'logs5.papertrailapp.com',
    LOG_PORT: process.env.LOG_PORT || 39491,
    SERVER_SECRET: process.env.SERVER_SECRET || 'checkmein2',
    TEST: '123'
};
